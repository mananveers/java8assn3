# Java8assn3

## qOne

This package contains two classes and an interface:

1. `FuncInter` interface: This interface contains an abstract method `cube()` that receives a float, three static methods `sub()`, `mul()`, and `div()` that each take two floats and return their difference, product, and quotient, respectively. It also contains two default methods `square()` and `sum()`.

2. `Implementor` class: This class implements the `FuncInter` interface and overrides the `cube()` method, defining it as a function that takes one float and returns its cube.

3. `Tester` class: This class defines an `Implementor` object and calls all of the methods in the `FuncInter` interface through it using user input.



## qTwo

This package contains two classes:

- `Person` class: This class stores details about a person, including the person's name and age. It has a parameterized constructor, getters/setters, and a display method that displays these details.
- `Tester` class: This class creates a Person object and displays the details of the object using a method reference.