package qTwo;

@FunctionalInterface
public interface FuncIter {
    Person create(String name, int age);
}

public class Tester {
    public static void main(String[] args) {
        FuncIter personSupplier = Person::new;
        personSupplier.create("John", 30).display();
    }
}
