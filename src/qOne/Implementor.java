package qOne;

@FunctionalInterface
public interface FuncInter {
    float cube(float num);

    static float sub(float a, float b) {
        return a - b;
    }

    static float mul(float a, float b) {
        return a * b;
    }

    static float div(float a, float b) {
        return a / b;
    }

    default float square(float num) {
        return num * num;
    }

    default float sum(float a, float b) {
        return a + b;
    }
}

public class Implementor implements FuncInter {
    @Override
    public float cube(float num) {
        return num * num * num;
    }
}
