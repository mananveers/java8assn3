package qOne;

import java.util.Scanner;

public class Tester {
    public static void main(String[] args) {
        Implementor implementor = new Implementor();
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter a number: ");
        float num = sc.nextFloat();

        System.out.println("Cube: " + implementor.cube(num));
        System.out.println("Square: " + implementor.square(num));

        System.out.print("Enter two numbers: ");
        float a = sc.nextFloat();
        float b = sc.nextFloat();

        System.out.println("Difference: " + FuncInter.sub(a, b));
        System.out.println("Product: " + FuncInter.mul(a, b));
        System.out.println("Quotient: " + FuncInter.div(a, b));
        System.out.println("Sum: " + implementor.sum(a, b));
    }
}
